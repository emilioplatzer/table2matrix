CREATE OR REPLACE FUNCTION table2matrix(p_source text, p_left_columns text, p_top_column text, p_data_column text, p_target text, p_create boolean, p_empty_before boolean) RETURNS text
  LANGUAGE PLpgSQL
AS
$BODY$
DECLARE
  v_get_top_columns text;
  v_create text;
  v_top_columns_and_types text;
  v_top_sums text;
  v_insert text;
  v_left_columns text:=p_left_columns;
  v_counts integer;
BEGIN
  v_get_top_columns:=format($$
    SELECT string_agg(distinct format('%%I DECIMAL',%I), ', ') as top_columns,
           string_agg(distinct format('sum(case when %%I=%%L then %%I end)',%L,%s,%L), ', ') as top_expr,
           count(*) as counts
      FROM %I $$,
    p_top_column,p_top_column,p_top_column,p_data_column,p_source
  );
  EXECUTE v_get_top_columns INTO v_top_columns_and_types,v_top_sums, v_counts;
  if v_counts = 0 then
    RAISE 'table2matrix p_source % is empty',quote_ident(p_source);
  end if;
  v_create:='CREATE TABLE '||quote_ident(p_target)||'('||
    (SELECT string_agg(quote_ident(name)||' '||quote_ident(type),', ') FROM (
        SELECT name, 'text'::text as type FROM regexp_split_to_table(p_left_columns,',') name
     ) x)||','||v_top_columns_and_types||'); ';
  v_insert:=format($$
    INSERT INTO %I 
      SELECT %s, %s
        FROM %I
        GROUP BY %s;$$,
    p_target, v_left_columns, v_top_sums, p_source, v_left_columns
  );
  IF p_create THEN
    IF p_empty_before THEN
      EXECUTE format('DROP TABLE %I',p_target);
    END IF;
    EXECUTE v_create;
  ELSE
    IF p_empty_before then
      EXECUTE format('DELETE FROM %I',p_target);
    END IF;
  END IF;
  EXECUTE v_insert;
  RETURN 'ok';
END;
$BODY$;

--TEST

do language plpgsql 
$sqls$
declare
  v_ok text;
  v_all jsonb;
  v_fails_good boolean:=false;
begin
  -- prepare fixture
  DROP TABLE IF EXISTS test_sales_matrix;
  DROP TABLE IF EXISTS test_sales;
  CREATE TABLE test_sales (years integer, items text, subsidiary text, total decimal);
  begin
    SELECT table2matrix('test_sales', 'years,items', 'subsidiary', 'total', 'test_sales_matrix', true, false) into v_ok;
  exception
    when others then
      v_fails_good:=true;
  end;
  if not v_fails_good then 
    RAISE 'expect to fail and not fails';
  end if;
  INSERT INTO test_sales values 
    (2018,'audio', 'Madrid', 39800),
    (2018,'audio', 'Paris', 359230),
    (2018,'media', 'Madrid', 73530),
    (2018,'media', 'Paris', 359230),
    (2017,'texts', 'Paris', 499230),
    (2018,'texts', 'Madrid', 94530);
  SELECT table2matrix('test_sales', 'years,items', 'subsidiary', 'total', 'test_sales_matrix', true, false) into v_ok;
  select jsonb_agg(to_jsonb(t.*) order by items,years) into v_all
    FROM test_sales_matrix t;
  IF v_all is distinct from '[{"Paris": 359230, "items": "audio", "years": "2018", "Madrid": 39800}, {"Paris": 359230, "items": "media", "years": "2018", "Madrid": 73530}, {"Paris": 499230, "items": "texts", "years": "2017", "Madrid": null}, {"Paris": null, "items": "texts", "years": "2018", "Madrid": 94530}]'::jsonb THEN
    raise 'not as espected %', v_all;
  END IF;
  SELECT table2matrix('test_sales', 'years,items', 'subsidiary', 'total', 'test_sales_matrix', false, false) into v_ok;
  select jsonb_agg(to_jsonb(t.*) order by items,years) into v_all
    FROM test_sales_matrix t;
  IF v_all is distinct from '[{"Paris": 359230, "items": "audio", "years": "2018", "Madrid": 39800}, {"Paris": 359230, "items": "audio", "years": "2018", "Madrid": 39800}, {"Paris": 359230, "items": "media", "years": "2018", "Madrid": 73530}, {"Paris": 359230, "items": "media", "years": "2018", "Madrid": 73530}, {"Paris": 499230, "items": "texts", "years": "2017", "Madrid": null}, {"Paris": 499230, "items": "texts", "years": "2017", "Madrid": null}, {"Paris": null, "items": "texts", "years": "2018", "Madrid": 94530}, {"Paris": null, "items": "texts", "years": "2018", "Madrid": 94530}]'::jsonb THEN
    raise 'not as espected %', v_all;
  END IF;
  SELECT table2matrix('test_sales', 'years,items', 'subsidiary', 'total', 'test_sales_matrix', false, true) into v_ok;
  select jsonb_agg(to_jsonb(t.*) order by items,years) into v_all
    FROM test_sales_matrix t;
  IF v_all is distinct from '[{"Paris": 359230, "items": "audio", "years": "2018", "Madrid": 39800}, {"Paris": 359230, "items": "media", "years": "2018", "Madrid": 73530}, {"Paris": 499230, "items": "texts", "years": "2017", "Madrid": null}, {"Paris": null, "items": "texts", "years": "2018", "Madrid": 94530}]'::jsonb THEN
    raise 'not as espected %', v_all;
  END IF;
  DROP TABLE IF EXISTS test_sales_matrix;
  DROP TABLE IF EXISTS test_sales;
end;
$sqls$;

-- select * from test_sales_matrix;