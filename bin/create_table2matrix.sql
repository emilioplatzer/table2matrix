CREATE OR REPLACE FUNCTION table2matrix(p_source text, p_left_columns text, p_top_column text, p_data_column text, p_target text) RETURNS text
  LANGUAGE PLpgSQL
AS
$BODY$
DECLARE
  v_get_top_columns text;
  v_create text;
  v_top_columns_and_types text;
  v_top_sums text;
  v_insert text;
  v_left_columns text:=p_left_columns;
BEGIN
  v_get_top_columns:=format($$
    SELECT string_agg(distinct format('%%I DECIMAL',%I), ', ') as top_columns,
           string_agg(distinct format('sum(case when %%I=%%L then %%I end)',%L,%s,%L), ', ') as top_expr
      FROM %I $$,
    p_top_column,p_top_column,p_top_column,p_data_column,p_source
  );
  EXECUTE v_get_top_columns INTO v_top_columns_and_types,v_top_sums;
  v_create:='CREATE TABLE '||quote_ident(p_target)||'('||
    (SELECT string_agg(quote_ident(name)||' '||quote_ident(type),', ') FROM (
        SELECT name, 'text'::text as type FROM regexp_split_to_table(p_left_columns,',') name
     ) x)||','||v_top_columns_and_types||'); ';
  v_insert:=format($$
    INSERT INTO %I 
      SELECT %s, %s
        FROM %I
        GROUP BY %s;$$,
    p_target, v_left_columns, v_top_sums, p_source, v_left_columns
  );
  EXECUTE v_create;
  EXECUTE v_insert;
  RETURN 'ok';
END;
$BODY$;

