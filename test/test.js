"use strict";

var fs = require('mz/fs');
var pg = require('pg');
var MiniTools = require('mini-tools');

describe("table2matrix", function(){
    var db;
    var finallyFun=function(){};
    before(()=>
        MiniTools.readConfig([
            'def-config.yaml',
            'local-config.yaml',
        ],{whenNotExist:'ignore'}).catch(function(err){
            console.log('problem reading def-config.yaml or local-config.yaml');
            throw err;
        }).then(config=>{
            console.log('begin testing in',config.db.database);
            finallyFun = () => db.query("SET SEARCH_PATH = "+config.db.schema);
            db = new pg.Client(config.db);
            return db.connect().then(()=>
                db.query("DROP SCHEMA IF EXISTS "+config.db.schema+" CASCADE")
            ).then(()=>
                db.query("CREATE SCHEMA "+config.db.schema)
            ).then(finallyFun);
        })
    );
    after(()=>db.end());
    it("test-all", ()=>
        fs.readFile("table2matrix.sql",{encoding:'utf8'}).then(
            sql=>db.query(sql)
        )
    );
    it("copy to bin", ()=>
        fs.readFile("table2matrix.sql",{encoding:'utf8'}).then(
            sql=>fs.writeFile("bin/create_table2matrix.sql",sql.split('--TEST')[0])
        )
    );
})