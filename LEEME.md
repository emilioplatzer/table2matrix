<!--multilang v0 es:LEEME.md en:README.md -->
# table2matrix

<!--lang:es-->
Transforma algo que podría ser una tabla en una tabla en formato matriz
<!--lang:en--]
Transform table like queries in denormalized table

[!--lang:*-->

<!--multilang buttons-->

idioma: ![castellano](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-es.png)
también disponible en:
[![inglés](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-en.png)](README.md)

<!--lang:es-->
# Caso de uso

Se necesita generar una tabla cuyas columnas dependan de los datos (en particular de las categorías de una columna)
de modo de pasar de formato tabla a formato matriz. 

Por ejemplo tenemos una tabla con las ventas por sucursal y por rubro 
y queremos transformarlo en una tabla donde cada sucursal sea una columna con las ventas de la sucursal.

<!--lang:en--]
# Case of use

You need to denormalize a table into a matrix like table. 

[!--lang:*-->

```sql
CREATE TABLE sales (years integer, items text, subsidiary text, total decimal);
INSERT INTO sales values 
  (2018,'audio', 'Madrid', 39800),
  (2018,'audio', 'Paris', 359230),
  (2018,'media', 'Madrid', 73530),
  (2018,'media', 'Paris', 359230),
  (2017,'texts', 'Paris', 499230),
  (2018,'texts', 'Madrid', 94530);
  
SELECT table2matrix('sales', 'years,items', 'subsidiary', 'total', 'sales_matrix');

SELECT * FROM sales_matrix;
```

years|items|Madrid|Paris
-----|-----|------|-----
2018 |audio| 39800|359230
2018 |media| 73530|359230
2017 |texts|      |499230
2018 |texts| 94530|

<!--lang:es-->
# Instalación

<!--lang:en--]
# Install

[!--lang:*-->

```sh
psql < bin/create_table2matrix.sql
```

<!--lang:es-->

## Licencia

<!--lang:en--]

## License

[!--lang:*-->

[MIT](LICENSE)

